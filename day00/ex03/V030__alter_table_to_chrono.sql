alter table indicators add column time_start  timestamp  default '01.01.1972' not null;
alter table indicators add column time_end  timestamp  default '01.01.9999' not null;


alter table country add column time_start  timestamp  default '01.01.1972' not null;
alter table country add column time_end  timestamp  default '01.01.9999' not null;
