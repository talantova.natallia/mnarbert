create sequence  seq_country
		increment by 10
		owned by country.id;
alter table data.country alter column id set default nextval('data.seq_country');
select setval('seq_country', (SELECT MAX(id) FROM data.country));

create sequence  seq_indicators
		increment by 10
		owned by indicators.id;
alter table indicators alter column id set default nextval('data.seq_indicators');
select setval('seq_indicators', (SELECT MAX(id) FROM data.indicators));

create sequence  seq_period
		increment by 10
		owned by period.id;
alter table period alter column id set default nextval('data.seq_period');
select setval('seq_period', (select max(id) from data.period));
