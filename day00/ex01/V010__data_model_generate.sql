insert into period(id, i_id, c_id, ref_date, value)
select row_number() over(),
       f.* from
	(select  (select id from indicators i 
			where i.name = 'Population of country') as i_id,
			c.id as c_id,
		  generate_series(
	           (date '2019-01-01')::timestamp,
	           (date '2019-12-31')::timestamp,
	           interval '1 month')::timestamp as ref_date,         
		  (random()*1000000)::integer  as value
		from country c
	      where c.object_type = 'country') f;

insert into period(id, i_id, c_id, ref_date, value)
select (row_number() over() + (select max(p.id) from "period" p)) as id,
	   f.* from
			(select  (select id from indicators i 
						where i.name = 'Unemployment rate') as i_id,
						c.id as c_id,
						generate_series(
				           (date '2019-01-01')::timestamp,
				           (date '2019-12-31')::timestamp,
				           interval '1 month'
				         )::timestamp as ref_date,         
						(random()*1000000)::integer  as value
				from country c
			      where c.object_type = 'country') f;
 
insert into period(id, i_id, c_id, ref_date, value)
 select (row_number() over() + (select max(p.id) from "period" p)) as id,
	  f.* from
		(select  (select id from indicators i 
				where i.name = 'Infected humans COVID-19 ') as i_id,
				c.id as c_id,
				generate_series(
		           (date '2019-01-01')::timestamp,
		           (date '2019-12-31')::timestamp,
		           interval '1 month'
		         )::timestamp as ref_date,         
				(random()*1000000)::integer  as value
			from country c
		      where c.object_type = 'country') f;
