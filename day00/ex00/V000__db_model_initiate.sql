create table indicators
( id   bigint not null,
  name varchar not null,
  unit varchar,
  constraint pk_indicators primary key(id),
  constraint uk_indicators_name unique(name)
);

comment on table indicators is 'information about different indicators or metrics';
comment on column indicators.id is 'primary key id for metrics';
comment on column indicators.name is 'unique name of the metric';
comment on column indicators.unit is 'description of metric''s value';


create table country
( id   bigint not null,
  name varchar not null, 
  object_type varchar not null default 'country', 
  par_id bigint,
  constraint pk_country primary key(id),
  constraint uk_country_name_object_type unique(name, object_type),
  constraint fk_par_id_country foreign key(par_id) references country(id)
);
comment on table country is 'information about all countries on the earth';
comment on column country.id is 'primary key id for object';
comment on column country.name is 'name of the object';
comment on column country.object_type is 'type of the object';
comment on column country.par_id is 'dependencies to the other objects in the table country';



create table period
( id   bigint not null,
  i_id bigint not null,
  c_id bigint not null,
  ref_date timestamp not null,
  value numeric,
  constraint pk_period primary key(id),
  constraint fk_id_indicators foreign key(i_id) references indicators(id),
  constraint fk_id_country foreign key(c_id) references country(id),
  constraint uk_i_id_c_id_ref_date unique(i_id, c_id, ref_date)
);

comment on table period is 'information about counted period and their values';
comment on column period.id is 'primary key id for object';
comment on column period.i_id is 'id of the table indicatiors';
comment on column period.c_id is 'id of the table country';
comment on column period.ref_date is 'date of the event';
comment on column period.value is 'value for period';

