
alter table indicators 
	add constraint ch_indicator_start 
	check(time_start >= '1972.01.01'::timestamp);
	
alter table indicators 
	add constraint ch_indicator_end
	check(time_end <= '9999.01.01'::timestamp);
	
alter table indicators 
	add constraint ch_indicator_unit 
	check (unit in ('human', 'percent'));
	
alter table country 
	add constraint ch_country_start 
	check(time_start >= '1972.01.01'::timestamp);
	
alter table country 
	add constraint ch_country_end
	check(time_end <= '9999.01.01'::timestamp);
	
alter table country 
	add constraint ch_country_type
	check(object_type in ('country','continent'));

alter table period 
	add constraint ch_period_value
	check(value >= 0);
	
alter table period alter column value set not null;
